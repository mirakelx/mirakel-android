package de.azapps.mirakel.new_ui.interfaces;

import de.azapps.mirakel.model.task.Task;

/**
 * Created by az on 05.08.14.
 */
public interface OnTaskSelectedListener {
	public void onTaskSelected(Task task);
}
