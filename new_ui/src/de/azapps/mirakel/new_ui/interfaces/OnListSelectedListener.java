package de.azapps.mirakel.new_ui.interfaces;

import de.azapps.mirakel.model.list.ListMirakel;
import de.azapps.mirakel.model.task.Task;


public interface OnListSelectedListener {
	public void onListSelected(ListMirakel list);
}
